# op_hmi_socket_client

This is a C++ windows based application , which connects -using sockets- to op_hmi_bridge node. It send user command and receive global planning information such as available destinations.

![](img/hmi_socket_client.png)

## HMI Architecture 

![](img/HMI_Beidge.png)