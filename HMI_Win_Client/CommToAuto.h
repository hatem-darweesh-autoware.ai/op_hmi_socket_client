#pragma once


#include "HMIMSG.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include<string>
#include <vector>
#include <sstream>


#define DEFAULT_PORT "10001"
#define DEFAULT_PROTO SOCK_STREAM
#define DEFAULT_BUFLEN 512
#define MAX_PACKET_SIZE 1000000

using namespace std;
using namespace PlannerHNS;

enum PacketTypes {

	INIT_CONNECTION = 0,
	ACTION_EVENT = 1,
};

struct Packet 
{
	unsigned int packet_type;
	void serialize(char * data) 
	{
		memcpy(data, this, sizeof(Packet));
	}

	void deserialize(char * data) 
	{
		memcpy(this, data, sizeof(Packet));
	}
};

class NetworkServices
{
public:
	static int sendMessage(SOCKET curSocket, char * message, int messageSize)
	{
		return send(curSocket, message, messageSize, 0);
	}
	static int receiveMessage(SOCKET curSocket, char * buffer, int bufSize)
	{
		return recv(curSocket, buffer, bufSize, 0);
	}
};

class CommToAuto
{
public:
	CommToAuto();
	virtual ~CommToAuto();
	int InitClient(string server_name, string port_send, string port_receive, CListBox* pList);
	void sendMessage(HMI_MSG msg);
	int ReConnect();
	int ReConnectClient();
	int receiveMessage(char *);
	bool DoOneClientStep();
	static void clientLoop(void * arg);

	SOCKET ListenSocket;
	SOCKET ClientSocket;
	SOCKET ConnectSocket;
	string m_serverName;
	string m_PortSend;
	string m_PortReceive;
	string m_LatestClientMsg;
	CListBox* m_pInfoBox;
	CButton* m_pLeftButton;
	CButton* m_pForwardButton;
	CButton* m_pRightButton;
	CListCtrl* m_pDestinationList;
	CImageList* m_pImageList;
	vector<DESTINATION> m_destinations;
	int m_nConnections;
};

