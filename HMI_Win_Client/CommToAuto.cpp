
#include "stdafx.h"
#include "CommToAuto.h"
#include <iostream>
#include <process.h>
#include <sstream>


CommToAuto::CommToAuto()
{
	m_pInfoBox = 0;
	m_pLeftButton = 0;
	m_pRightButton = 0;
	m_pForwardButton = 0;	
	m_nConnections = 0;
}


CommToAuto::~CommToAuto()
{
}

int CommToAuto::ReConnect()
{
	WSADATA wsaData;
	ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,  *ptr = NULL, hints;

	// Initialize Winsock
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);

	if (iResult != 0) 
	{
		printf("WSAStartup failed with error: %d\n", iResult);
		return -1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP; 

	//resolve server address and port 
	iResult = getaddrinfo(m_serverName.c_str(), m_PortReceive.c_str(), &hints, &result);

	if (iResult != 0)
	{
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return  -1;
	}

	// Attempt to connect 
	for (ptr = result; ptr != NULL;ptr = ptr->ai_next) 
	{
		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

		if (ConnectSocket == INVALID_SOCKET) 
		{
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return  -1;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);

		if (iResult == SOCKET_ERROR)
		{
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			printf("The server is down... did not connect");
		}
	}

	freeaddrinfo(result);
	if (ConnectSocket == INVALID_SOCKET)
	{
		printf("Unable to connect to server!\n");
		WSACleanup();
		return  -1;
	}

	u_long iMode = 1;
	iResult = ioctlsocket(ConnectSocket, FIONBIO, &iMode);
	if (iResult == SOCKET_ERROR)
	{
		printf("ioctlsocket failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return  -1;
	}
	char value = 1;
	setsockopt(ConnectSocket, IPPROTO_TCP, TCP_NODELAY, &value, sizeof(value));

	return 1;
}
int CommToAuto::ReConnectClient()
{
	WSADATA wsaData;
	ClientSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL, *ptr = NULL, hints;

	// Initialize Winsock
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);

	if (iResult != 0) 
	{
		printf("WSAStartup failed with error: %d\n", iResult);
		return -1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;  
	//resolve server address and port 
	iResult = getaddrinfo(m_serverName.c_str(), m_PortSend.c_str(), &hints, &result);

	if (iResult != 0)
	{
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return  -1;
	}

	// Attempt to connect
	for (ptr = result; ptr != NULL;ptr = ptr->ai_next) 
	{
		ClientSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

		if (ClientSocket == INVALID_SOCKET) 
		{
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return  -1;
		}

		// Connect to server.
		iResult = connect(ClientSocket, ptr->ai_addr, (int)ptr->ai_addrlen);

		if (iResult == SOCKET_ERROR)
		{
			closesocket(ClientSocket);
			ClientSocket = INVALID_SOCKET;
			printf("The server is down... did not connect");
			return -1;
		}
	}

	freeaddrinfo(result);
	if (ClientSocket == INVALID_SOCKET)
	{
		printf("Unable to connect to server!\n");
		WSACleanup();
		return  -1;
	}

	u_long iMode = 1;
	iResult = ioctlsocket(ClientSocket, FIONBIO, &iMode);
	if (iResult == SOCKET_ERROR)
	{
		printf("ioctlsocket failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return  -1;
	}

	char value = 1;
	setsockopt(ClientSocket, IPPROTO_TCP, TCP_NODELAY, &value, sizeof(value));

	return 1;
}

int CommToAuto::InitClient(string server_name, string port_send, string port_receive, CListBox* pList)
{
	m_pInfoBox = pList;
	m_serverName = server_name;
	m_PortSend = port_send;
	m_PortReceive = port_receive;
	_beginthread(CommToAuto::clientLoop, 0, this);
	return 1;
}

void CommToAuto::sendMessage(HMI_MSG msg)
{
	if (ReConnectClient() >= 0)
	{
		if (ClientSocket != INVALID_SOCKET)
		{
			string final_str = msg.CreateStringMessage();
			NetworkServices::sendMessage(ClientSocket, (char*)final_str.c_str(), final_str.size());
			shutdown(ClientSocket, 0);
			closesocket(ClientSocket);
			WSACleanup();
		}
	}
}

int CommToAuto::receiveMessage(char * recvbuf)
{
	int iResult = NetworkServices::receiveMessage(ConnectSocket, recvbuf, MAX_PACKET_SIZE);
	if (iResult == 0)
	{
		printf("Connection closed\n");
		shutdown(ConnectSocket, 0);
		closesocket(ConnectSocket);
		WSACleanup();
		return -1;
	}

	return iResult;
}

bool CommToAuto::DoOneClientStep()
{
	if (ReConnect() < 0)
	{
		m_pForwardButton->EnableWindow(false);
		m_pLeftButton->EnableWindow(false);
		m_pRightButton->EnableWindow(false);
		return false;
	}

	Sleep(10);

	Packet packet;
	char network_data[MAX_PACKET_SIZE];
	int data_length = receiveMessage(network_data);

	if (data_length <= 0)
	{
		m_pForwardButton->EnableWindow(false);
		m_pLeftButton->EnableWindow(false);
		m_pRightButton->EnableWindow(false);
	}
	else
	{

		m_LatestClientMsg = string(network_data, data_length);
		HMI_MSG msg = HMI_MSG::FromString(m_LatestClientMsg);
		if (m_pInfoBox->GetCount() > 50)
		{
			m_pInfoBox->DeleteString(m_pInfoBox->GetCount() - 1);
		}
		m_LatestClientMsg = "Message>" + m_LatestClientMsg;
		m_pInfoBox->InsertString(0, LPCTSTR(m_LatestClientMsg.c_str()));

		if (msg.type == UNKNOWN_MSG)
		{
		}
		else if (msg.type == DESTINATIONS_MSG)
		{
			if (msg.destinations.size() > 0)
			{
				HMI_MSG cnf_msg;
				cnf_msg.msg_id = msg.msg_id;
				cnf_msg.type = CONFIRM_MSG;
				cnf_msg.bErr = 0;
				sendMessage(cnf_msg);
			}
			//AfxMessageBox(m_LatestClientMsg.c_str());
			m_destinations.clear();
			m_pDestinationList->DeleteAllItems();
			int current_index = 0;
			for (unsigned int i = 0; i < msg.destinations.size(); i++)
			{
				if (msg.destinations.at(i).id == msg.curr_destination_id)
					current_index = i;

				m_destinations.push_back(msg.destinations.at(i));
				std::ostringstream oss;
				oss << msg.destinations.at(i).id << " | ";
				oss << msg.destinations.at(i).name << "\n";
				oss << msg.destinations.at(i).hour << ":";
				oss << msg.destinations.at(i).minute;

				std::string str = oss.str();
				LVITEM* lvItem = new LVITEM;
				lvItem->mask = LVIF_IMAGE | LVIF_TEXT;
				lvItem->iItem = i;
				lvItem->iSubItem = 0;
				lvItem->pszText = LPSTR(str.c_str());
				lvItem->iImage = 1;
				m_pDestinationList->InsertItem(lvItem);
			}
			//m_pDestinationList->SetHotItem(current_index);
			m_pDestinationList->SetSelectionMark(current_index);
		}
		else if (msg.type == OPTIONS_MSG)
		{
			bool bEnableForward = false;
			bool bEnableLeft = false;
			bool bEnableRight = false;
			bool bDestinationReached = false;

			for (unsigned int i = 0; i < msg.available_actions.size(); i++)
			{

				if (msg.available_actions.at(i) == MSG_FORWARD_ACTION)
					bEnableForward = true;
				else if (msg.available_actions.at(i) == MSG_LEFT_TURN_ACTION)
					bEnableLeft = true;
				else if (msg.available_actions.at(i) == MSG_RIGHT_TURN_ACTION)
					bEnableRight = true;
				else if (msg.available_actions.at(i) == MSG_DESTINATION_REACHED)
					bDestinationReached = true;
			}
			
			m_pForwardButton->EnableWindow(bEnableForward);
			m_pLeftButton->EnableWindow(bEnableLeft);
			m_pRightButton->EnableWindow(bEnableRight);

			if (bDestinationReached)
			{
				for (unsigned int i = 0; i < m_destinations.size(); i++)
				{
					if (i < m_pDestinationList->GetItemCount())
						m_pDestinationList->SetItemText(i, 0, LPCTSTR("Arrived"));

					if (m_destinations.at(i).id == msg.curr_destination_id)
					{
						break;	
					}
				}

			}
			
			for (unsigned int i = 0; i < m_destinations.size(); i++)
			{
				if (m_destinations.at(i).id == msg.curr_destination_id)
				{
					m_pDestinationList->SetHotItem(i);
					break;
				}
			}

		}
	}

	if (m_destinations.size() == 0) // No Destinations , request destinations from HMI server 
	{
		HMI_MSG dst_msg;
		dst_msg.msg_id = 1;
		dst_msg.type = DESTINATIONS_MSG;
		dst_msg.bErr = 0;
		sendMessage(dst_msg);
	}
	return true;
}

void CommToAuto::clientLoop(void * arg)
{
	CommToAuto* pCom = (CommToAuto*)arg;
	pCom->m_nConnections = 1;
	while (1)
	{
		if (!pCom->DoOneClientStep())
		{
			if (pCom->m_pInfoBox->GetCount() > 5)
			{
				pCom->m_pInfoBox->DeleteString(pCom->m_pInfoBox->GetCount() - 1);
			}
			
			pCom->m_pInfoBox->InsertString(0, "Server down !");
			Sleep(50);
		}
	}

	pCom->m_nConnections = 0;
}


